#!/bin/bash

#parameters
FILE="aviso"
DATA="important_file"

while [ true ]; do
  if test -f "$FILE"; then
      #echo "ALERT"
      echo "Sending emergency message to IPFS"
      date > log
      echo -e "\nSending unencrypted data to IPFS, alert was sent from local" >> log
      #ipfs add "$DATA"

      #I know I should not fo this:
      exit
  fi
  sleep 5
done
