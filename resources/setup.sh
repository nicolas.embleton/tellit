#!/bin/bash

DNAME="tellit_data" #change name of the data directory here
DDIR=" "            #change name of the default location here

notify-send Setup -t 10 "Creating main directory..."
cd $DDIR
mkdir $DNAME
cd $DNAME
notify-send Setup -t 10 "Created!"
