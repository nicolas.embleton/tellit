#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
NC='\033[0m' # No Color

#COMAN=$1
FILE=$2
error="You forgot something! Use -h for help"

#tellit-c -pripu -o key_name
#tellit-c -e public_key -i file -o file_2
#tellit-c -d private_key -i file -o file_2
#tellit-c -tm
#tellit-c -h
#tellit-c -bu directory -p passphrase


#pripu
function create_pubpri {
  #create key pair using rsa
  dir=""$NAME"_id-"$(( ( RANDOM % 99 )  + 100 ))""
  mkdir $dir
  cd $dir
  echo -e "${PURPLE}Creating a private key...${NC}"
  openssl genrsa -out "$NAME".pem 1024
  echo -e "${PURPLE}Creating a public key...${NC}"
  openssl rsa -in "$NAME".pem -pubout > "$NAME".pub
  cd ..
}

#ipfs
function ipfs_start {
  #before using this, you need to configure it with the ipfs_config script
  echo -e "${PURPLE}Initializing IPFS daemon... wait a moment${NC}"
  sleep 6
  ipfs daemon &
  echo -e "Done."
}


#main
if [ -z "$1" ]; then #1
  echo $error

elif [ "$1" == "--pripu" ]; then
  if [ -z "$2" ]; then #2
    echo $error
  elif [ "$2" == "-o" ]; then
    if [ -z "$3" ]; then #3
      echo $error
    else
      NAME="$3"
      create_pubpri
    fi #3
  else #-o is not there
    #maybe this error is redundant ??? #issue here
    echo $error
  fi #2

#tellit-c -e public_key -i file -o file_2
elif [ "$1" == "-e" ]; then
  if [ "$3" == "-i" ] && [ "$5" == "-o" ]; then
    notify-send Tellit -t 20 "Encrypting your file... | 公開鍵暗号いる..."
    openssl rsautl -encrypt -pubin -inkey $2.pub -ssl -in $4 -out "$6"
  else
    echo $error
  fi

#tellit-c -d private_key -i file -o file_2
elif [ "$1" == "-d" ]; then
  if [ "$3" == "-i" ] && [ "$5" == "-o" ]; then
    notify-send Tellit -t 20 "Decrypting your file... | 解読いる"
    openssl rsautl -decrypt -in $4 -out "$6" -inkey "$2".pem
  else
    echo $error
  fi

#tellit-c -bu directory -p passphrase
elif [ "$1" == "--bu" ]; then
  if [ "$3" == "-p" ]; then
    #backup directpry
    BU=""$2"_backup"
    mkdir $BU
    cd $2
    #copy archives in backup
    for i in *; do cp $i ../$BU; done
    cd ../$BU
    #encrypt and remove originals in backup
    notify-send Tellit -t 20 "Encrypting your backup...| バックアップの暗号いろ..."
    for f in *; do openssl enc -aes-256-cbc -salt -in $f -out "$f".enc -k $4; rm $f; done
    cd ..
  else
    echo $error
  fi

elif [ "$1" == "--tm" ]; then
  cd ..
  cd resources
  ./check.sh &

elif [ "$1" == "--ipfs" ]; then
  #check if add or get
  #ipfs_start
  if [ "$2" == "--add" ]; then
    if [ "$4" == "--pass" ]; then
      #ipfs add $3
      openssl enc -aes-256-cbc -salt -in $3 -out "$3".enc -k $5
      #echo -e "Adding encrypted document..."
    #  notify-send Tellit -t 20 "Adding encrypted document to IPFS..."
      ipfs add "$3".enc
      #echo -e "${BLUE}Done.${NC}"
    #  notify-send Tellit -t 20 "Done."
      rm "$3".enc

    elif [ "$4" == "-e" ]; then
      openssl rsautl -encrypt -pubin -inkey $5.pub -ssl -in $3 -out "$3".enc
    #  notify-send Tellit -t 20 "Adding encrypted document to IPFS..."
      ipfs add "$3".enc
    #  notify-send Tellit -t 20 "Done."
      rm "$3".enc

    else
      echo $error
    fi

  elif [ "$2" == "--get" ]; then
    ipfs get $3
  else
    echo $error
  fi

elif [ "$1" == "-h" ]; then
  echo -e "These are the commands:\n\n${PURPLE}For creating a key pair:${NC}\ntellit-c --pripu -o key_name\n\n${BLUE}For encrypt a file using the public key:${NC}\ntellit-c -e public_key -i file -o file_2\n\n${PURPLE}For decrypting a file using a private key${NC}\ntellit-c -d public_key -i file -o file_2\n\n${BLUE}For activating the TellMe mode:${NC}\ntellit-c --tm 20m\n\n${PURPLE}For adding encrypted content to ipfs:${NC}\ntellit-c --ipfs --add document --pass password\n\ntellit-c --ipfs --add document -e public_key\n\n${BLUE}For getting an encrypted document from ipfs (You must know the hash):${NC}\ntellit-c --ipfs --get hash\n(or use 'ipfs get hash' in any terminal)"

else
  echo $error
  echo "Did you forget the command?"
fi #1
