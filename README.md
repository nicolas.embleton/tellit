# Tellit

A Linux command-line tool for journalists. Share information encrypted, upload to IPFS, deadman pedal and more.

報道ためのLinuxのコマンドラインインターフェイスのツール。暗号メール報道を共有。IPFSのコンテントをアップ。デッドマンペダルなど

![](https://gitlab.com/terceranexus6/tellit/raw/master/multimedia/Gifi.gif)

## How to use

The instructions (in English and Japanese) are in [the wiki](https://gitlab.com/terceranexus6/tellit/wikis/Command-line) apart from relevant information related to the project, such as the motivation.

Use **help** and create a keypair:

![](https://gitlab.com/terceranexus6/tellit/raw/master/multimedia/example1.gif)

Use encrypt:

![](https://gitlab.com/terceranexus6/tellit/raw/master/multimedia/example2.gif)

Use decrypt:

![](https://gitlab.com/terceranexus6/tellit/raw/master/multimedia/example3.gif)

Use ipfs:

![](https://gitlab.com/terceranexus6/tellit/raw/master/multimedia/example4.gif)

![](https://gitlab.com/terceranexus6/tellit/raw/master/multimedia/example5.gif)

## Licenced under GPLv3

All the code here is licenced under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).

![](https://www.gnu.org/graphics/lgplv3-with-text-154x68.png)
